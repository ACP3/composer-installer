<?php

namespace ACP3\Composer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;
use React\Promise\PromiseInterface;

class Installer extends LibraryInstaller
{
    private const ACP3_BASE = 'acp3-base';
    private const ACP3_CORE = 'acp3-core';
    private const ACP3_MODULE = 'acp3-module';
    private const ACP3_THEME = 'acp3-theme';

    /**
     * Contains the supported types of the various ACP3 components
     *
     * @var array
     */
    private static $packageTypes = [
        self::ACP3_BASE,
        self::ACP3_CORE,
        self::ACP3_MODULE,
        self::ACP3_THEME,
    ];

    /**
     * Contains the code locations
     *
     * @var array
     */
    private static $locations = [
        self::ACP3_MODULE => 'ACP3/Modules/',
        self::ACP3_CORE => '',
        self::ACP3_THEME => 'designs/',
    ];

    /**
     * @param \Composer\Package\PackageInterface $package
     *
     * @return string
     */
    public function getInstallPath(PackageInterface $package)
    {
        $type = $package->getType();

        // Use composer's default path for storing the acp3-base - we will handle that later
        if ($type === self::ACP3_BASE) {
            return parent::getInstallPath($package);
        }

        $basePath = self::$locations[$type];

        $path = '';

        // Add some special path logic for ACP3 modules
        if ($type === self::ACP3_MODULE) {
            $path = $this->getModulePath($package->getPrettyName());
        }

        // As composer packages are not allowed to contain uppercase characters add a little workaround for this
        $extra = $package->getExtra();
        if (!empty($extra['installer-name'])) {
            $path = $extra['installer-name'];
        }

        // Check for a valid name
        if (empty($path) && $type !== self::ACP3_CORE) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Could not install %s with the name %s because it has an invalid name.',
                    substr($type, 5),
                    $package->getPrettyName()
                )
            );
        }

        return $basePath . $path;
    }

    /**
     * Adjusts the module path
     *
     * @param string $packageName
     *
     * @return string
     */
    private function getModulePath(string $packageName): string
    {
        if (strpos($packageName, '/') !== false) {
            [$vendor, $moduleName] = explode('/', $packageName);

            return ucfirst($vendor) . '/' . ucfirst($moduleName);
        }

        return '';
    }

    /**
     * {@inheritDoc}
     */
    public function supports(string $packageType)
    {
        return \in_array($packageType, self::$packageTypes, true);
    }

    /**
     * {@inheritdoc}
     */
    public function install(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        $installBase = function() use ($package) {
            $this->installACP3Base($package);
        };

        $promise = parent::install($repo, $package);

        // Composer v2 might return a promise here
        if ($promise instanceof PromiseInterface) {
            return $promise->then($installBase);
        }

        $installBase();

        return null;
    }

    /**
     * Copy files of the acp3-base package to the project root
     *
     * @param PackageInterface $package
     */
    private function installACP3Base(PackageInterface $package): void
    {
        if ($package->getType() !== self::ACP3_BASE) {
            return;
        }

        $extra = $package->getExtra();

        if (!isset($extra['map'])) {
            return;
        }

        foreach ($extra['map'] as $filePath) {
            $pathInfo = \pathinfo($filePath);
            $srcPath = $this->vendorDir . '/' . $package->getPrettyName() . '/' . $filePath;

            if (false === file_exists($pathInfo['dirname']) && !mkdir($concurrentDirectory = $pathInfo['dirname'], 0755, true) && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }

            \copy($srcPath, $filePath);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function update(InstalledRepositoryInterface $repo, PackageInterface $initial, PackageInterface $target)
    {
        $updateBase = function() use ($initial, $target) {
            $this->deleteOldACP3Base($initial);
            $this->installACP3Base($target);
        };

        $promise = parent::update($repo, $initial, $target);

        // Composer v2 might return a promise here
        if ($promise instanceof PromiseInterface) {
            return $promise->then($updateBase);
        }

        $updateBase();

        return null;
    }

    /**
     * Cleans up the previously installed (older) version of the acp3/base package
     *
     * @param PackageInterface $package
     */
    private function deleteOldACP3Base(PackageInterface $package): void
    {
        if ($package->getType() !== self::ACP3_BASE) {
            return;
        }

        $extra = $package->getExtra();

        if (!isset($extra['map'])) {
            return;
        }

        foreach ($extra['map'] as $filePath) {
            if (is_file($filePath)) {
                \unlink($filePath);
            }
        }
    }
}
