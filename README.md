# ACP3 custom Composer installer
Adds a custom installer to Composer so that it can install ACP3 modules into the right location.

# Development

## Installation

```shell
$ docker run --rm --interactive --tty \
  --volume $PWD:/app \
  --user $(id -u):$(id -g) \
  composer install
```
